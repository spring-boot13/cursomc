package com.nelioalves.cursomc.services;

import com.nelioalves.cursomc.domain.PagamentoComBoleto;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class BoletoService {

    /**
     * Usado somente para preencher a data de vencimento.
     * @param pagamentoComBoleto
     * @param instante
     */
    public void preencherPagamentoComBoleto(PagamentoComBoleto pagamentoComBoleto, Date instante) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(instante);
        calendar.add(Calendar.DAY_OF_MONTH, 7);
        pagamentoComBoleto.setDataVencimento(calendar.getTime());
    }

}
