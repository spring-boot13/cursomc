package com.nelioalves.cursomc.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

@Service
public class MockEmailService extends AbstractEmailService {

    private static final Logger log = LoggerFactory.getLogger(MockEmailService.class);

    @Override
    public void sendEmail(SimpleMailMessage simpleMailMessage) {
        log.info("Simulando envio de e-mail");
        log.info(simpleMailMessage.toString());
        log.info("E-mail enviado");
    }

    @Override
    public void sendHtmlEmail(MimeMessage mimeMessage) {
        log.info("Simulando envio de e-mail html");
        log.info(mimeMessage.toString());
        log.info("E-mail enviado");
    }

}
