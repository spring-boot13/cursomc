package com.nelioalves.cursomc.services;

import com.nelioalves.cursomc.security.UserSS;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserService {

    public static UserSS authenticated() {
        try {
            final UserSS userSS = (UserSS)
                    SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            return userSS;
        } catch (Exception ex) {
            return null;
        }
    }

}
