package com.nelioalves.cursomc.services;

import com.nelioalves.cursomc.domain.Cliente;
import com.nelioalves.cursomc.domain.ItemPedido;
import com.nelioalves.cursomc.domain.PagamentoComBoleto;
import com.nelioalves.cursomc.domain.Pedido;
import com.nelioalves.cursomc.domain.enums.EstadoPagamento;
import com.nelioalves.cursomc.repositories.ClienteRepository;
import com.nelioalves.cursomc.repositories.ItemPedidoRepository;
import com.nelioalves.cursomc.repositories.PagamentoRepository;
import com.nelioalves.cursomc.repositories.PedidoRepository;
import com.nelioalves.cursomc.security.UserSS;
import com.nelioalves.cursomc.services.exceptions.AuthorizationException;
import com.nelioalves.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PedidoService {

    @Autowired
    private PedidoRepository pedidoRepository;

    @Autowired
    private BoletoService boletoService;

    @Autowired
    private ProdutoService produtoService;

    @Autowired
    private ItemPedidoRepository itemPedidoRepository;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ClienteRepository clienteRepository;

    public Pedido find(Integer id) {
        return this.pedidoRepository.findById(id)
                .orElseThrow(() ->
                        new ObjectNotFoundException("Objeto não encontrado! id: " +
                                id + ", Tipo: " + Pedido.class.getSimpleName()));
    }

    public Pedido insert(Pedido entity) {
        entity.setId(null);
        entity.setInstante(new Date());
        entity.getPagamento().setEstado(EstadoPagamento.PENDENTE);
        entity.getPagamento().setPedido(entity);

        if (entity.getPagamento() instanceof PagamentoComBoleto) {
            PagamentoComBoleto pagamento = (PagamentoComBoleto) entity.getPagamento();
            this.boletoService.preencherPagamentoComBoleto(pagamento, entity.getInstante());
        }

        entity = this.pedidoRepository.save(entity);
        this.pagamentoRepository.save(entity.getPagamento());
        for (ItemPedido item : entity.getItens()) {
            item.setDesconto(0.0);
            item.setPreco(this.produtoService.find(item.getProduto().getId()).getPreco());
            item.setPedido(entity);
        }
        this.itemPedidoRepository.saveAll(entity.getItens());
        this.emailService.sendOrderConfirmationHtmlEmail(entity);
        return entity;
    }

    public Page<Pedido> findPaged(Integer page, Integer linesPerPage, String direction, String orderBy) {
        final UserSS user = UserService.authenticated();
        if (user == null) {
            throw new AuthorizationException("Acesso negado");
        }
        final PageRequest pageRequest =
                PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        final Cliente cliente = this.clienteRepository.findById(user.getId()).get();
        return this.pedidoRepository.findByCliente(cliente, pageRequest);
    }

}
