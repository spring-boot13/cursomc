package com.nelioalves.cursomc.services;

import com.nelioalves.cursomc.domain.Categoria;
import com.nelioalves.cursomc.dto.CategoriaDTO;
import com.nelioalves.cursomc.repositories.CategoriaRepository;
import com.nelioalves.cursomc.services.exceptions.DataIntegrityException;
import com.nelioalves.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    public Categoria find(Integer id) {
        return this.categoriaRepository.findById(id)
                .orElseThrow(() ->
                        new ObjectNotFoundException("Objeto não encontrado! id: " +
                                id + ", Tipo: " + Categoria.class.getSimpleName()));
    }

    public Categoria insert(Categoria categoria) {
        categoria.setId(null);
        return this.categoriaRepository.save(categoria);
    }

    public Categoria update(Categoria categoria) {
        Categoria categoriaDB = find(categoria.getId());
        updateData(categoriaDB, categoria);
        return this.categoriaRepository.save(categoria);
    }

    public void delete(Integer id) {
        find(id);
        try {
            this.categoriaRepository.deleteById(id);
        } catch (DataIntegrityViolationException ex) {
            throw new DataIntegrityException("Não é possível excluir uma categoria que possua produtos.");
        }
    }

    public List<Categoria> findAll() {
        List<Categoria> categorias = this.categoriaRepository.findAll();
        return categorias;
    }

    public Page<Categoria> findPaged(Integer page, Integer linesPerPage, String direction, String orderBy) {
        PageRequest pageRequest =
                PageRequest.of(page, linesPerPage, Sort.Direction.valueOf(direction), orderBy);
        return this.categoriaRepository.findAll(pageRequest);
    }

    public Categoria fromDTO(CategoriaDTO dto) {
        return new Categoria(dto.getId(), dto.getNome());
    }

    protected void updateData(Categoria categoriaDB, Categoria categoria) {
        categoriaDB.setNome(categoria.getNome());
    }

}
