package com.nelioalves.cursomc.services.validation;

import com.nelioalves.cursomc.domain.Cliente;
import com.nelioalves.cursomc.domain.enums.TipoCliente;
import com.nelioalves.cursomc.dto.ClienteNewDTO;
import com.nelioalves.cursomc.repositories.ClienteRepository;
import com.nelioalves.cursomc.resources.exceptions.FieldMessage;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClienteInsertValidator implements ConstraintValidator<ClienteInsert, ClienteNewDTO> {

    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public void initialize(ClienteInsert ann) {
    }

    @Override
    public boolean isValid(ClienteNewDTO dto, ConstraintValidatorContext context) {
        final List<FieldMessage> errors = new ArrayList<>();
        final String regex = "([0-9]{2}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[\\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\\.]?[0-9]{3}[\\.]?[0-9]{3}[-]?[0-9]{2})";

        if (dto.getTipo().equals(TipoCliente.PESSOAFISICA.getCodigo()) && !dto.getCpfOuCnpj().matches(regex)) {
            errors.add(new FieldMessage("cpfOuCnpj", "CPF inválido"));
        }

        if (dto.getTipo().equals(TipoCliente.PESSOAJURIDICA.getCodigo()) && !dto.getCpfOuCnpj().matches(regex)) {
            errors.add(new FieldMessage("cpfOuCnpj", "CNPJ inválido"));
        }

        final Cliente cliente = this.clienteRepository.findByEmail(dto.getEmail());
        if (Objects.nonNull(cliente)) {
            errors.add(new FieldMessage("email", "Email já existente"));
        }

        for (FieldMessage e : errors) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(e.getMessage())
                    .addPropertyNode(e.getFieldName()).addConstraintViolation();
        }
        return errors.isEmpty();
    }

}
