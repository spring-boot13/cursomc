package com.nelioalves.cursomc.services;

import com.nelioalves.cursomc.domain.Cliente;
import com.nelioalves.cursomc.repositories.ClienteRepository;
import com.nelioalves.cursomc.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AuthService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EmailService emailService;

    private Random random = new Random();

    public void sendNewPassword(final String email) {
        final Cliente cliente = this.clienteRepository.findByEmail(email);
        if (cliente == null) {
            throw new ObjectNotFoundException("Email not found");
        }
        final String newPass = newPassword();
        cliente.setSenha(this.bCryptPasswordEncoder.encode(newPass));
        this.clienteRepository.save(cliente);
        this.emailService.sendNewPasswordEmail(cliente, newPass);
    }

    private String newPassword() {
        char[] vet = new char[10];
        for (int i = 0; i < vet.length; i++) {
            vet[i] = randomChar();
        }
        return new String(vet);
    }

    private char randomChar() {
        int opt = random.nextInt(3);
        if (opt == 0) { // gera um dígito
            return (char) (random.nextInt(10) + 48);
        } else if (opt == 1) { // gera letra maiúscula
            return (char) (random.nextInt(26) + 65);
        } else { // gera letra minúscula
            return (char) (random.nextInt(26) + 97);
        }
    }

}
