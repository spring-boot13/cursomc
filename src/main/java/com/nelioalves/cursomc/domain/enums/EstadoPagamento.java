package com.nelioalves.cursomc.domain.enums;

import java.util.Arrays;
import java.util.Objects;

public enum EstadoPagamento {

    PENDENTE(1, "Pendente"),
    QUITADO(2, "Quitado"),
    CANCELADO(3, "Cancelado");

    private Integer codigo;
    private String descricao;

    EstadoPagamento(Integer codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public static EstadoPagamento toEnum(Integer cod) {
        if (Objects.isNull(cod)) {
            return null;
        }
        return Arrays.asList(EstadoPagamento.values()).stream()
                .filter(tc -> tc.getCodigo().equals(cod))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
