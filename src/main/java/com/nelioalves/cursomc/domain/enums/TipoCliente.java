package com.nelioalves.cursomc.domain.enums;

import java.util.Arrays;
import java.util.Objects;

public enum TipoCliente {

    PESSOAFISICA(1, "Pessoa Física"),
    PESSOAJURIDICA(2, "Pessoa Jurídica");

    private Integer codigo;
    private String descricao;

    TipoCliente(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public static TipoCliente toEnum(Integer cod) {
        if (Objects.isNull(cod)) {
            return null;
        }
        return Arrays.asList(TipoCliente.values()).stream()
                .filter(tc -> tc.getCodigo().equals(cod))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
