package com.nelioalves.cursomc.domain.enums;

import java.util.Arrays;
import java.util.Objects;

public enum Perfil {

    ADMIN(1, "ROLE_ADMIN"),
    CLIENTE(2, "ROLE_CLIENTE");

    private Integer codigo;
    private String descricao;

    Perfil(int codigo, String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public static Perfil toEnum(Integer cod) {
        if (Objects.isNull(cod)) {
            return null;
        }
        return Arrays.asList(Perfil.values()).stream()
                .filter(tc -> tc.getCodigo().equals(cod))
                .findFirst().orElseThrow(IllegalArgumentException::new);
    }

}
