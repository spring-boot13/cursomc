package com.nelioalves.cursomc.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nelioalves.cursomc.dto.CredenciaisDTO;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

// intercepta requisição de login (/login)
// esse filtro é registrado no SecurityConfig
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private JWTUtil jwtUtil;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try {
            final CredenciaisDTO credenciaisDTO =
                    new ObjectMapper().readValue(request.getInputStream(), CredenciaisDTO.class);
            final UsernamePasswordAuthenticationToken authToken =
                    new UsernamePasswordAuthenticationToken(credenciaisDTO.getEmail(),
                            credenciaisDTO.getSenha());
            final Authentication auth = this.authenticationManager.authenticate(authToken);
            return auth;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        final String username = ((UserSS) authResult.getPrincipal()).getUsername();
        final String token = this.jwtUtil.generateToken(username);
        response.addHeader("Authorization", "Bearer " + token);
    }
}
