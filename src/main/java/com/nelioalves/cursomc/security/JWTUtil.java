package com.nelioalves.cursomc.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    public String generateToken(final String userName) {
        return Jwts.builder()
                .setSubject(userName)
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(SignatureAlgorithm.HS512, secret.getBytes())
                .compact();
    }

    public String getUsername(final String token) {
        final Claims claims = getClaims(token);
        if (claims != null) {
            return claims.getSubject();
        }
        return null;
    }

    public boolean tokenValido(final String token) {
        final Claims claims = getClaims(token);
        if (claims != null) {
            final String username = claims.getSubject();
            final Date expiration = claims.getExpiration();
            final Date now = new Date(System.currentTimeMillis());
            if (username != null && expiration != null && now.before(expiration)) {
                return true;
            }
        }
        return false;
    }

    private Claims getClaims(final String token) {
        try {
            return Jwts
                    .parser()
                    .setSigningKey(this.secret.getBytes())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception ex) {
            return null;
        }
    }

}
