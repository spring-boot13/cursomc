package com.nelioalves.cursomc.resources;

import com.nelioalves.cursomc.domain.Cliente;
import com.nelioalves.cursomc.dto.ClienteDTO;
import com.nelioalves.cursomc.dto.ClienteNewDTO;
import com.nelioalves.cursomc.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/clientes")
public class ClienteResource {

    @Autowired
    private ClienteService clienteService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cliente> listar(@PathVariable("id") Integer id) {
        final Cliente cliente = this.clienteService.find(id);
        return ResponseEntity.ok().body(cliente);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> update(@Valid @RequestBody ClienteDTO dto, @PathVariable("id") Integer id) {
        dto.setId(id);
        Cliente categoria = this.clienteService.fromDTO(dto);
        this.clienteService.update(categoria);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {
        this.clienteService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<ClienteDTO>> findAll() {
        final List<Cliente> categorias = this.clienteService.findAll();
        final List<ClienteDTO> dtos = categorias.stream().map(ClienteDTO::new).collect(Collectors.toList());
        return ResponseEntity.ok().body(dtos);
    }

    /*
     http://localhost:8080/categorias/paged?linesPerPage=3&page=1&direction=DESC
     */
    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/paged", method = RequestMethod.GET)
    public ResponseEntity<Page<ClienteDTO>> findPaged(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction,
            @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy) {
        Page<Cliente> categorias = this.clienteService.findPaged(page, linesPerPage, direction, orderBy);
        Page<ClienteDTO> dtos = categorias.map(ClienteDTO::new);
        return ResponseEntity.ok().body(dtos);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@Valid @RequestBody ClienteNewDTO dto) {
        Cliente cliente = this.clienteService.fromDTO(dto);
        cliente = this.clienteService.insert(cliente);

        // fromCurrentRequest pega a url que foi usada para inserir
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(cliente.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

}
