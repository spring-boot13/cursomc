package com.nelioalves.cursomc.resources;

import com.nelioalves.cursomc.domain.Produto;
import com.nelioalves.cursomc.dto.ProdutoDTO;
import com.nelioalves.cursomc.resources.utils.URL;
import com.nelioalves.cursomc.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/produtos")
public class ProdutoResource {

    @Autowired
    private ProdutoService produtoService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Produto> listar(@PathVariable("id") Integer id) {
        final Produto produto = this.produtoService.find(id);
        return ResponseEntity.ok().body(produto);
    }

    /*
     http://localhost:8080/produtos/paged/?nome=or&categorias=1,4
     */
    @RequestMapping(value = "/paged", method = RequestMethod.GET)
    public ResponseEntity<Page<ProdutoDTO>> findPaged(
            @RequestParam(value = "nome", defaultValue = "") String nome,
            @RequestParam(value = "categorias", defaultValue = "") String categorias,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction,
            @RequestParam(value = "orderBy", defaultValue = "nome") String orderBy) {
        List<Integer> ids = URL.decodeIntList(categorias);
        String nomeDecoded = URL.decodeParam(nome);
        Page<Produto> produtos = this.produtoService.search(nomeDecoded, ids, page, linesPerPage, direction, orderBy);
        Page<ProdutoDTO> dtos = produtos.map(ProdutoDTO::new);
        return ResponseEntity.ok().body(dtos);
    }

}
