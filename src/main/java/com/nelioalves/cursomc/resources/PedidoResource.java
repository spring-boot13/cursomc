package com.nelioalves.cursomc.resources;

import com.nelioalves.cursomc.domain.Pedido;
import com.nelioalves.cursomc.services.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping(value = "/pedidos")
public class PedidoResource {

    @Autowired
    private PedidoService pedidoService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Pedido> listar(@PathVariable("id") Integer id) {
        final Pedido pedido = this.pedidoService.find(id);
        return ResponseEntity.ok().body(pedido);
    }

    /*
        {
            "cliente" : {"id" : 1},
            "enderecoDeEntrega" : {"id" : 1},"pagamento" : {
            "numeroDeParcelas" : 10,
            "@type": "pagamentoComCartao"
            },
            "itens" : [
            {
            "quantidade" : 2,
            "produto" : {"id" : 3}
            },
            {
            "quantidade" : 1,
            "produto" : {"id" : 1}
            }
            ]
        }
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> insert(@Valid @RequestBody Pedido entity) {
        entity = this.pedidoService.insert(entity);

        // fromCurrentRequest pega a url que foi usada para inserir
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(entity.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    // traz somente o pedido do usuário logado
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page<Pedido>> findPaged(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "linesPerPage", defaultValue = "24") Integer linesPerPage,
            @RequestParam(value = "direction", defaultValue = "DESC") String direction,
            @RequestParam(value = "orderBy", defaultValue = "instance") String orderBy) {
        Page<Pedido> categorias = this.pedidoService.findPaged(page, linesPerPage, direction, orderBy);
        return ResponseEntity.ok().body(categorias);
    }

}
